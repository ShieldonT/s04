# class
class Camper():
    def __init__(self,name,batch,course_type):
        self.name = name
        self.batch = batch
        self.course_type = course_type
    # methods
    def careertrack(self):
        print(f'Currently enrolled in the {self.course_type} program.')
    def info(self):
        print(f'My name is {self.name} of batch {self.batch}.')
# new instance
zuitt_camper = Camper("Alex","4","python short course")
print(f'Camper Name : {zuitt_camper.name}.')
print(f'Camper Batch : {zuitt_camper.batch}.')
print(f'Camper Course : {zuitt_camper.course_type}.')
zuitt_camper.info()
zuitt_camper.careertrack()




    